from django.http import response
from rest_framework import status
from rest_framework.test import APITestCase , APIClient
from rest_framework.authtoken.models import Token

from accounts.models import User


class UserViewTest(APITestCase):
    def setUp(self):
        
        self.client = APIClient()
        
        self.common_data = {
            "username": "common",
            "first_name": "John",
            "last_name": "Wick",
            "password": "1234",
            "is_staff": False,
            "is_superuser": False,
            }
        
        self.common_data_2 = {
            "username": "common",
            "first_name": "John",
            "last_name": "Wick",
            "password": "1234",
            "is_staff": False,
            "is_superuser": False,
            }
        
        self.critic_data = {
            "username": "critic",
            "first_name": "Jacques",
            "last_name": "Aumont",
            "password": "1234",
            "is_staff": True,
            "is_superuser": False,
            }
        
        self.critic_data_2 = {
            "username": "critic",
            "first_name": "Jacques2",
            "last_name": "Aumont2",
            "password": "1234",
            "is_staff": True,
            "is_superuser": False,
            }
        
        self.admin_data = {
            "username": "admin",
            "first_name": "John3",
            "last_name": "Wick3",
            "password": "1234",
            "is_staff": True,
            "is_superuser": True,
            }
        
        self.admin_data_2 = {
            "username": "admin",
            "first_name": "John3",
            "last_name": "Wick3",
            "password": "1234",
            "is_staff": True,
            "is_superuser": True,
            }
        
        self.common_login_data = {"username": "common", "password": "1234"}
        
        self.critic_login_data = {"username": "critic", "password": "1234"}

        self.admin_login_data = {"username": "admin", "password": "1234"}
        
        self.admin_login_fail_data = {"username": "admin", "password": "123456"}
        
        self.movie_data = {
                "title": "O Poderoso Chefão 2",
                "duration": "175m",
                "genres": [
                    {"name": "Crime"},
                    {"name": "Drama"}
                ],
                "premiere": "1972-09-10",
                "classification": 14,
                "synopsis": "Don Vito Corleone (Marlon Brando) é o chefe de uma 'família' ..."
            }
        
        self.movie_data_2 = {
            "title": "Um Sonho de Liberdade",
            "duration": "142m",
            "genres": [
                {"name": "Drama"},
                {"name": "Ficção científica"}
            ],
            "premiere": "1994-10-14",
            "classification": 16,
            "synopsis": "Andy Dufresne é condenado a duas prisões perpétuas..."
        }

        self.movie_data_3 = {
                "title": "Em busca da liberdade",
                "duration": "175m",
                "genres": [
                    {"name": "Drama"},
                    {"name": "Obra de época"}
                ],
                "premiere": "2018-02-22",
                "classification": 14,
                "synopsis": "Representando a Grã-Bretanha,  corredor Eric Liddell",
            }

        self.movie_fail_data = {
                "title": "O Poderoso Chefão 2",
                "duration": "175m",
                "genres": [
                    {"name": "Crime"},
                    {"name": "Crime"}
                ],
                "premiere": "1972-09-10",
                "classification": 14,
                "synopsis": "Don Vito Corleone (Marlon Brando) é o chefe de uma 'família' ..."
            }
        
        self.update_movie_data = {
                "title": "O Poderoso Chefão",
                "duration": "175m",
                "genres": [
                    {"name": "Crime"},
                    {"name": "Drama"},
                    {"name": "Mafia"}
                ],
                "premiere": "1972-09-10",
                "classification": 14,
                "synopsis": "Don Vito Corleone (Marlon Brando) é o chefe de uma 'família' ..."
            }
        
        self.review_data = {
                "stars": 7,
                "review": "O Poderoso Chefão 2 podia ter dado muito errado...",
                "spoilers": False,
            }
        
        self.review_data_2 = {
                "stars": 12,
                "review": "Wolverine dies in the end...",
                "spoilers": True,
            }
        
        self.url_accounts = f"/api/accounts/"
        self.url_login = f"/api/login/"
        self.url_movie = f"/api/movies/"
        self.url_update_movie = f"/api/movies/1/"
        self.url_create_review = f"/api/movies/1/review/"
        self.url_get_review = f"/api/reviews/"
        self.url_delete_movie = f"/api/movies/1/"

    def test_create_user_common(self):
        
        expectation = {
                "id": 1,
                "username": "common",
                "first_name": "John",
                "last_name": "Wick",
                "is_superuser": False,
                "is_staff": False,
            }
        
        # cria o usuario
        response = self.client.post(self.url_accounts, self.common_data)
        self.assertEqual(response.status_code, 201)
        
        #verifica se o usuario foi criado corretamente
        self.assertEqual(self.common_data.json(), expectation)
        
        # falha ao criar usuario repetido
        response = self.client.post(self.url_accounts, self.common_data_2)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"username": ["A user with that username already exists."]}
        )

    def test_create_user_critic(self):
        expectation = {
                "id": 1,
                "username": "critic",
                "first_name": "John",
                "last_name": "Wick",
                "is_superuser": False,
                "is_staff": True,
            }
        
        # cria o usuario
        response = self.client.post(self.url_accounts, self.critic_data)
        self.assertEqual(response.status_code, 201)
        
        #verifica se o usuario foi criado corretamente        
        self.assertEqual(self.critic_data.json(), expectation)
        
        # falha ao criar usuario repetido
        response = self.client.post(self.url_accounts, self.critic_data_2)
        self.assertEqual(response.status_code, 400)
        self.assertEqual( response.json(), {"username": ["A user with that username already exists."]})

    def test_create_user_admin(self):
        expectation =  {
                "id": 1,
                "username": "admin",
                "first_name": "John",
                "last_name": "Wick",
                "is_superuser": True,
                "is_staff": True,
            }

        # cria o usuario
        response = self.client.post(self.url_accounts, self.admin_data)
        self.assertEqual(response.status_code, 201)
        
        #verifica se o usuario foi criado corretamente
        self.assertEqual( self.admin_data.json(), expectation)
        
        # falha ao criar usuario
        response = self.client.post(self.url_accounts, self.admin_data_2)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"username": ["A user with that username already exists."]})

    def test_login_success(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        response = self.client.post(self.url_login, self.admin_login_data)
        self.assertEqual(response.status_code, 200)
        
        # retorno do token
        self.assertIn('token', response.json())
       
    def test_login_fail(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        response = self.client.post(self.url_login, self.admin_login_fail_data)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {"detail": "You do not have permission to perform this action."})

    def test_admin_create_movie_sucess(self):
        
        # user = User.objects.create(**self.admin_data)
        # token = Token.objects.get_or_create(user=user)[0]
        
        expectation = {
                "id": 1,
                "title": "O Poderoso Chefão 2",
                "duration": "175m",
                "genres": [
                    {"id": 1, "name": "Crime"},
                    {
                        "id": 2, "name": "Drama"}
                ],
                "premiere": "1972-09-10",
                "classification": 14,
                "synopsis": "Don Vito Corleone (Marlon Brando) é o chefe de uma ..."
            }
         
        self.client.post(self.url_accounts, self.admin_data) 
            
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # admin cria um filme
        response = self.client.post(self.url_movie, self.movie_data)
        
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json(), expectation)
        
    def test_admin_create_movie_fail(self):
         
        self.client.post(self.url_accounts, self.admin_data) 
               
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # admin cria um filme com generos repetido
        response = self.client.post(self.url_movie, self.movie_fail_data)
        
        self.assertEqual(response.status_code, 400)


    def test_user_critic_cannot_create_a_movie(self):
        
        self.client.post(self.url_accounts, self.critic_data) 
               
        token =self.client.post(self.url_login, self.critic_login_data).json()['token']

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # critic nao pode criar filme
        response = self.client.post(self.url_movie, self.movie_data)
        self.assertEqual(response.status_code , 403)

    def test_user_common_cannot_create_a_movie(self):
        
        self.client.post(self.url_accounts, self.common_data)
               
        token =self.client.post(self.url_login, self.common_login_data).json()['token']

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # common nao pode criar filme
        response = self.client.post(self.url_movie, self.movie_data)
        self.assertEqual(response.status_code, 403)

    def test_admin_updating_movie_sucess(self):
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # cria o filme
        response = self.client.post(self.url_movie, self.movie_data)
        self.assertEqual(response.status_code, 201)
        
        #atualiza filme
        response = self.client.put(self.url_update_movie, self.update_movie_data)
        self.assertEqual(response.status_code, 200)
        
        #checagem dos dados
        for k, v in self.update_movie_data.items():
            self.assertEquals(v, response.data[k])

    def test_fail_updating_movie(self):
        # cria usuario admin e loga
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # cria filme
        response = self.client.post(self.url_movie, self.movie_data)
        self.assertEqual(response.status_code, 201)
        
        self.client.credentials()
        
        # cria usuario critic e loga
        self.client.post(self.url_accounts, self.critic_data)
        
        token =self.client.post(self.url_login, self.critic_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
                
        response = self.client.post(self.url_movie, self.movie_data)
        self.assertEqual(response.status_code, 403)

    def test_if_an_invalid_movie_id_is_passed(self):
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # cria o filme
        response = self.client.post(self.url_movie, self.movie_data)
        self.assertEqual(response.status_code, 201)
        
        #atualiza filme
        movie = self.client.put("/api/movies/99", self.update_movie_data)
        self.assertEqual(movie.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Not found."})

    def test_creating_review(self):
        # cria admin e loga
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        #cria filme para review
        self.client.post(self.url_movie, self.movie_data)
        
        self.client.credentials()
        
        # cria critic e loga
        self.client.post(self.url_accounts, self.critic_data)
        token =self.client.post(self.url_login, self.critic_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        response = self.client.post(self.url_create_review, self.review_data)
        
        expectation = {
                "id": 1,
                "critic": {
                    "id": 1,
                    "first_name": "Jacques",
                    "last_name": "Aumont"
                },
                "stars": 7,
                "review": "O Poderoso Chefão 2 podia ter dado muito errado...",
                "spoilers": False,
            }

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json(), expectation)
        self.assertEqual(len(response["reviews"]), 1)

    def test_review_already_created(self):
        # cria admin e loga
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        #cria filme para review
        self.client.post(self.url_movie, self.movie_data)
        
        self.client.credentials()
        
        # loga e cria a review
        self.client.post(self.url_accounts, self.critic_data)
        token =self.client.post(self.url_login, self.critic_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        response = self.client.post(self.url_create_review, self.review_data)
        self.assertEqual(response.status_code, 201)

        # caso o critic ja tenha feito uma review
        response = self.client.post(self.url_create_review, self.review_data)
        self.assertEqual(response.status_code, 422)
        self.assertEqual(response.json(), {"detail": "You already made this review."})
        
    def test_if_an_invalid_movie_id_is_passed(self):
        
        # cria admin e loga
        self.client.post(self.url_accounts, self.admin_data) 
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        #cria filme para review
        self.client.post(self.url_movie, self.movie_data)
        
        self.client.credentials() # limpando credenciais
        
        # loga e cria a review com movie_id invalido
        self.client.post(self.url_accounts, self.critic_data)
        token =self.client.post(self.url_login, self.critic_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

        response = self.client.post("/api/movies/99/review/", self.review_data)
        
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Not found."})
    
    def test_number_of_invalid_stars(self):
        # cria admin e loga
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        #cria filme para review
        self.client.post(self.url_movie, self.movie_data)
        
        self.client.credentials()
        
        # loga e cria a review
        self.client.post(self.url_accounts, self.critic_data)
        token =self.client.post(self.url_login, self.critic_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        response = self.client.post(self.url_create_review, self.review_data_2)
        
        self.assertEqual(response.status_code, 400)
        
    def test_route_that_changes_a_review_already_performed(self):

        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # cria o filme
        response = self.client.post(self.url_movie, self.movie_data)
        self.assertEqual(response.status_code, 201)
        
        self.client.credentials()
        
        # loga e cria a review
        self.client.post(self.url_accounts, self.critic_data)
        token =self.client.post(self.url_login, self.critic_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        review = self.client.post(self.url_create_review, self.review_data)
        
         #atualiza review
        
        update_data = {
            "stars": 2,
            "review": "O Poderoso Chefão 2 podia ter dado muito certo..",
            "spoilers": True
        }
        
        response = self.client.put("/api/movies/1/review/", self.update_data)
        self.assertEqual(response.status_code, 200)
        
        for k, v in update_data.items():
            self.assertEquals(v, response.data[k])

    def test_admin_get_review(self):
        # cria admin e loga
        self.client.post(self.url_accounts, self.admin_data) 
        
        token = self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # cria o filme
        self.client.post(self.url_movie, self.movie_data)
        
        self.client.credentials()
        
        # loga e cria a review
        self.client.post(self.url_accounts, self.critic_data)
        token = self.client.post(self.url_login, self.critic_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        self.client.post(self.url_create_review, self.review_data)
        
        self.client.credentials()
        
        token = self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        #testa o get
        response = self.client.get(self.url_get_review)
        self.assertEqual(response.status_code, 200)

    def test_critic_get_review(self):
        # cria admin e loga
        self.client.post(self.url_accounts, self.admin_data) 
        
        token = self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # cria o filme
        self.client.post(self.url_movie, self.movie_data)
        
        self.client.credentials()
        
        # loga e cria a review
        self.client.post(self.url_accounts, self.critic_data)
        token = self.client.post(self.url_login, self.critic_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        self.client.post(self.url_create_review, self.review_data)
        
        #testa o get
        response = self.client.get(self.url_get_review)
        self.assertEqual(response.status_code, 200)
         

    def test_commum_get_review(self):
        # cria admin e loga
        self.client.post(self.url_accounts, self.admin_data) 
        
        token = self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        # cria o filme
        self.client.post(self.url_movie, self.movie_data)
        
        self.client.credentials()
        
        # loga e cria a review
        self.client.post(self.url_accounts, self.critic_data)
        token = self.client.post(self.url_login, self.critic_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        self.client.post(self.url_create_review, self.review_data)
        
        self.client.credentials()
        
        token = self.client.post(self.url_login, self.common_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        #testa o get
        response = self.client.get(self.url_get_review)        
        self.assertEqual(response.status_code, 403)
    
    def test_that_lists_all_registered_movies(self):
        self.client.post(self.url_accounts, self.admin_data)
        
        token = self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        movie = self.client.post(self.url_movie, self.movie_data)
        
        movie_2 = self.client.post(self.url_movie, self.movie_data_2)
        
        movie_3 = self.client.post(self.url_movie, self.movie_data_3)
        
        self.client.credentials()
        
        response = self.client.get(self.url_movie)
        self.assertEqual(response.status_code, 200)
        
    
    def test_that_lists_all_registered_movies_based_on_request_filtering(self):
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        movie = self.client.post(self.url_movie, self.movie_data)
        
        movie_2 = self.client.post(self.url_movie, self.movie_data_2)
        
        movie_3 = self.client.post(self.url_movie, self.movie_data_3)
        
        self.client.credentials()
        
        filtered_movie = self.client.get("/api/movies/?title=liberdade")

        self.assertEqual(filtered_movie.status_code, 200)
        self.assertEqual(len(filtered_movie.json()), 2)
        
    def test_that_fetches_the_movie_specified_by_id(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        movie = self.client.post(self.url_movie, self.movie_data)
        
        movie_2 = self.client.post(self.url_movie, self.movie_data_2)

        expectation = {
            "id": 1,
            "title": "O Poderoso Chefão 2",
            "duration": "175m",
            "genres": [
                {
                    "id": 1,
                    "name": "Crime"
                },
                {
                    "id": 2,
                    "name": "Drama"
                }
            ],
            "premiere": "1972-09-10",
            "classification": 14,
            "synopsis": "Don Vito Corleone (Marlon Brando) é o chefe de uma ..."
        }
        
        self.client.credentials()

        filtered_movie = self.client.get("/api/movies/1")
        
        self.assertEqual(filtered_movie.status_code, 200)
        self.assertEqual(filtered_movie.json(), expectation)
        
        
    def test_that_searches_the_specified_movie_by_id_with_authenticated_user(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        movie = self.client.post(self.url_movie, self.movie_data)
        
        movie_2 = self.client.post(self.url_movie, self.movie_data_2)

        expectation = {
                "id": 1,
                "title": "Nomadland",
                "duration": "110m",
                "genres": [
                    {"id": 1, "name": "Drama"},
                    {"id": 2, "name": "Obra de Época"}
                ],
                "premiere": "2021-04-15",
                "classification": 14,
                "synopsis": "Uma mulher na casa dos 60 anos que, depois de perder...",
                "reviews": [
                    {
                    "id": 1,
                    "critic": {"id": 1, "first_name": "Jacques", "last_name": "Aumont"
                    },
                    "stars": 7,
                    "review": "Nomadland apresenta fortes credenciais para ser favorito ...",
                    "spoilers": False
                    }
                ]
            }
        self.client.credentials()
        
        filtered_movie = self.client.get("/api/movies/1")
        
        self.assertEqual(filtered_movie.status_code, 200)
        self.assertEqual(filtered_movie.json(), expectation)
        
    def test_if_an_invalid_movie_id_is_passed(self):
        self.client.post(self.url_accounts, self.admin_data)
        
        token = self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        self.client.post(self.url_movie, self.movie_data)
        
        movie_id_ivalid = self.client.get("/api/movies/99")
        
        self.assertEqual(movie_id_ivalid.status_code, 404)
        
    def test_route_that_deletes_movies(self):
        self.client.post(self.url_accounts, self.admin_data)
        
        token = self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        #cria um filme
        self.client.post(self.url_movie, self.movie_data)
        
        response = self.client.delete(self.url_delete_movie)
        
        self.assertAlmostEqual(response.status_code, 204)
        
    def test_route_that_deletes_movies_with_invalid_movie_id(self):
        self.client.post(self.url_accounts, self.admin_data)
        
        token = self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        response = self.client.delete("/api/movies/99")
        
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Not found."})