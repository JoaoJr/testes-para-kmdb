from django.test import TestCase

from accounts.models import User

class UserModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.username = "User"
        cls.first_name = "John"
        cls.last_name = "Doe"
        cls.password = "456"
        cls.is_staff = True
        cls.is_superuser = True
        
        cls.user = User.objects.create(
            username=cls.username,
            first_name=cls.first_name,
            last_name=cls.last_name,
            password=cls.password,
            is_staff=cls.is_staff,
            is_superuser=cls.is_superuser
        )
        
    def test_user_has_information_fields(self):
        self.assertIsInstance(self.user.username, str)
        self.assertEqual(self.user.username, self.username)
        
        self.assertIsInstance(self.user.first_name, str)
        self.assertEqual(self.user.first_name, self.first_name)
        
        self.assertIsInstance(self.user.last_name, str)
        self.assertEqual(self.user.last_name, self.last_name)
        
        self.assertIsInstance(self.user.password, str)
        self.assertEqual(self.user.password, self.password)
        
        self.assertIsInstance(self.user.is_staff, bool)
        self.assertEqual(self.user.is_staff, self.is_staff)
        
        self.assertIsInstance(self.user.is_superuser, bool)
        self.assertEqual(self.user.is_superuser, self.is_superuser)
        
    